<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required|max:255'
        ]);
        $query = DB::table('pertanyaan')->insert([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);

        return redirect('/pertanyaan')->with('sukses', 'Pertanyaan Berhasil Dibuat');
    }

    public function show($pertanyaan_id)
    {
        $pertanyaan_show = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.show', compact('pertanyaan_show'));
    }

    public function edit($pertanyaan_id)
    {
        $pertanyaan_edit = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();

        return view('pertanyaan.edit', compact('pertanyaan_edit'));
    }

    public function update(Request $request, $pertanyaan_id)
    {
        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required|max:255'
        ]);
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);

        return redirect('/pertanyaan')->with('sukses', 'Pertanyaan Berhasil Diubah');
    }

    public function destroy($pertanyaan_id)
    {
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        return redirect('/pertanyaan')->with('sukses', 'Pertanyaan Berhasil Dihapus');
    }
}
