@extends('master')

@section('content')
<!-- general form elements -->
<div class="ml-3 mt-3 mr-1">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Buat Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" method="POST" action="/pertanyaan">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul','') }}" placeholder="Tuliskan Judul" required>
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi','') }}" placeholder="Tuliskan Pertanyaan" required>
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
    <a href="/pertanyaan" class="btn btn-primary btn-sm">Kembali</a>
    <!-- /.card -->
</div>

@endsection