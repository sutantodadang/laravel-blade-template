@extends('master')

@section('content')
<div class="ml-3 mt-3 mr-1">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Tabel Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('sukses'))
            <div class="alert alert-success">
                {{session('sukses')}}
            </div>
            @endif
            <a class="btn btn-primary mb-2 btn-sm" href="/pertanyaan/create">Buat Pertanyaan Baru</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">No</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th style="width: 40px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($pertanyaan as $i => $pertanyaan)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{$pertanyaan->judul}}</td>
                        <td>{{$pertanyaan->isi}}</td>
                        <td style="display: flex">
                            <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm mr-1">Show</a>
                            <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-primary btn-sm mr-1">Ubah</a>
                            <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">No Data</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
            </ul>
        </div> -->
    </div>
    <!-- /.card -->
</div>

@endsection