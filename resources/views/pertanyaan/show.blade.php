@extends('master')

@section('content')
<div class="ml-3 mt-3 mr-1">
    <h3 align="center">{{$pertanyaan_show->judul}}</h3>
    <p>{{$pertanyaan_show->isi}}</p>
</div>
<div class="ml-3 mt-3 mr-1">
    <a href="/pertanyaan" class="btn btn-primary btn-sm">Kembali</a>
</div>

@endsection